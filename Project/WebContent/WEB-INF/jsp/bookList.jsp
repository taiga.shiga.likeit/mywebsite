<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>予約一覧</title>
    <link rel="stylesheet" href="css/travelSearchResul.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                     <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <h2>予約一覧</h2>

    <div class="seciton">
        <c:forEach var="book" items="${bookList}">
        <div class="card">
            <div class="card_image">
                <a href="FavoriteDetail?postId=${book.id}"><img src="img/${book.mainImgName}"></a>
            </div>
            <div class="hosting_content">
                <span class="hosting-type">建物スタイル <br>
                ${book.buildingId}</span>
                <p>貸し出しスタイル ${book.roomId}</p>
            </div>
        </div>
</c:forEach>
        </div>
</body></html>
