<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/home.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <%--   <p style="float: left; padding-left: 200px; padding-top: 5px;">ようこそ ${user.name} さん</p> --%>
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <form action="Home" method="post">
        <h1 class="yourhost">あなたが、いきたい場所のホストを探してみよう</h1>
        <div class="form-box">
            <input type="text" class="search-location" placeholder="location" name="location">
            <p>チェックイン</p>
            <input type="date" class="checkin-day" name="checkIn">
            <p>チェックアウト</p>
            <input type="date" class="checkout-day" name="checkOut">
            <p>ゲストの数</p>
            <input type="number" class="peopleNumber" min="1" max="5" value=1 name="guestNum">
            <c:if test="${errMsg != null}">
            <p style="color:red;">${errMsg}</p>
            </c:if>
            <button type="submit" class="search-btn">探す</button>
        </div>
    </form>
</body></html>