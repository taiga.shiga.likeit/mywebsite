<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/styles.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>
    <!--image-->
    <div>
    <c:if test="${errMsg != null }">
     <script>
     alert('${errMsg}');
     </script>
    </c:if>

        <p class="image1"><img src="img/people_PNG.png" alt="人々のイメージ"></p>
        <!--ログイン-->
        <div class="login-box">
            <form action="LoginResult" method="post">
                <h1>Login</h1>
                <div class="textbox">
                    <input type="text" placeholder="LoginID" name="login_id"  class="text1">
                </div>
                <div class="textbox">
                    <input type="text" placeholder="Password" name="password" class="text2">
                </div>
                <button class="button btn1" type=submit name="action">ログイン</button>
            </form>
            <a href="Regist"><button class="btn2" type=submit name="action">新規登録</button></a>
        </div>
    </div>
</body></html>
