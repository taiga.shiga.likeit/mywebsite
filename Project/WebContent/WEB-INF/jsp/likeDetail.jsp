<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>お気に入り詳細</title>
<link rel="stylesheet" href="css/travelDetail.css">
</head>

<body>
	<header>
		<div class="container">
			<div class="header-list">
				<nav>
					<a href="Home">HOME</a> <a href="Post1">POST</a> <a href="Message">MESSAGE</a>
					<a href="Profile">PROFILE</a> <a href="Regist">NEWUSER</a> <a
						href="Logout">LOGOUT</a>
					<div class="animation start-home"></div>
				</nav>
			</div>
		</div>
	</header>

	<div class="buildingtype">
		<h1>志賀さんのアパート</h1>
	</div>
	<p class="roomtype">個室タイプ</p>

	<div class="result_img">
		<img id=0 class="preview normal" src="img/${post.mainImgName}"
			alt="preview"><br />
		<c:forEach items="${imageList}" var="image" varStatus="loop">
			<img id="${loop.count}" class="thumb normal"
				src="img/${image.imageName}" alt="natural${loop.count}"
				onmouseover="preview(this)">
		</c:forEach>
	</div>

	<div class="box">
			<div class="group">
				<p>お気に入りから外しますか？</p>
				<a href="FavoriteDelate?postId=${post.id}"><button type="button" class="nextbutton"
						onclick="validation()">削除する</button></a>
			</div>

			<div class="messagebox">
				<p>ここに宿泊したいですか？</p>
				<a href="#"><button type="submit" class="nextbutton">ホストにメッセージを送る</button></a>
			</div>
	</div>

	<hr align=left>

	<h1 class="amenity">利用可能なアメニティや設備</h1>
	<div class="amenitybox">
	<c:forEach var="amenity" items="${amenityList}">
      <button type="button" class="amenitybutton">${amenity.itemName}</button>
     </c:forEach>
	</div>

	<hr align=left>

	<div class="staydate">
		<h1>宿泊可能期間</h1>
  <p>${post.checkIn} ~ ${post.checkOut}</p>
 	</div>

	<hr align=left>

	<h1 style="margin-left: 150px;">ホストからのメッセージ</h1>

	<p class="messageforguest">
		<textarea name="" cols="500" rows="10" placeholder="" readonly>${post.message}</textarea>
	</p>

	<script>
        var lastImg = 1;
        document.getElementById(lastImg).className = "thumb selected";

        function preview(img) {
            document.getElementById(lastImg).className = "thumb normal";
            img.className = "thumb slected";
            document.getElementById(0).src = img.src;
            lastImg = img.id;
        }
        //アラート
        function validation() {
            alert('お気に入りが削除されました');
        }
    </script>
</body>
</html>