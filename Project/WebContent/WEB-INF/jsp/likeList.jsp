<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>お気に入り一覧</title>
    <link rel="stylesheet" href="css/travelSearchResul.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <h2>お気に入り一覧</h2>

    <div class="seciton">
    <c:forEach var="like" items="${likeList}">
        <div class="card">
            <div class="card_image">
                <a href="FavoriteDetail?postId=${like.id}"><img src="img/${like.mainImgName}"></a>
            </div>
            <div class="hosting_content">
                <span class="hosting-type">建物スタイル ${like.buildingId}</span>
                <p>貸し出しスタイル ${post.roomId}</p>
            </div>
        </div>
</c:forEach>
 </div>
</body></html>
