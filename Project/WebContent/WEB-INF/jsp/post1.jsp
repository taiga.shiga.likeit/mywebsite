<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>投稿</title>
    <link rel="stylesheet" href="css/post1.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>
<form action="Post1Result" method="post">
    <h1 Style="text-align: center;">お部屋の掲載をしよう</h1>
    <div class="buildingType">
        <p>カテゴリー</p>
        <select name="buildingType_id">
         <c:forEach var = "bType" items="${buildingType}">
            <option value="${bType.buildingType}">${bType.buildingType}</option>
            </c:forEach>
        </select>
    </div>
    <div class="roomType">
        <p>ゲストが利用可能な、お部屋のタイプは</p>
        <select name="roomType_id">
          <c:forEach var = "rType" items="${roomType}">
            <option value="${rType.roomType}">${rType.roomType}</option>
            </c:forEach>
        </select>
    </div>
    <div class="people-number">
        <p>最大定員</p>
        <select name="number">
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
        </select>
    </div>
    <div class="nextbox">
        <a href="Post1Result"><button type="submit" class="next">次へ</button></a>
    </div>
    </form>
</body></html>
