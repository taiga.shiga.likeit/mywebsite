<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/post2.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                   <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <h1 Style="text-align: center">掲載する場所の所在地</h1>
     <c:if test="${errMsg != null}">
       <p Style="text-align: center; color: red">${errMsg}</p>
    </c:if>
<form action="Post2Result" method="post">
    <div class="postalcode">
        <p>郵便番号</p>
        <input type="text" name="postalNum" placeholder="例)123-4567" value="${postalNum}">
    </div>
    <div class="address">
        <p Style="text-align: center">住所</p>
        <input type="text" name="address" placeholder="例）東京都港区芝公園４丁目１０−１７" value="${address}">
    </div>

    <div class="keyword">
        <p Style="text-align: center">ゲストが検索するときに使われるキーワードを設定</p>
        <input type="text" name="keyWord" placeholder="例）大阪難波、東京新宿、大宮など" value="${keyWord}">
    </div>
    <div class="buttonbox">
        <div class="backbox">
            <a href="Post1"><button type="button" class="backbutton">戻る</button></a>
        </div>
        <div class="nextbox">
            <a href="Post2Result"><button type="submit" class="nextbutton">次へ</button></a>
        </div>
    </div>
    </form>
</body></html>
