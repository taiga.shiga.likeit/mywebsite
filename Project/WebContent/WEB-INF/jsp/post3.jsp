<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<title>post</title>
<link rel="stylesheet" href="css/post3.css">
</head>

<body>
	<header>
		<div class="container">
			<div class="header-list">
				<nav>
					<a href="Home">HOME</a> <a href="Post1">POST</a> <a href="Message">MESSAGE</a>
					<a href="Profile">PROFILE</a> <a href="Regist">NEWUSER</a> <a
						href="Logout">LOGOUT</a>
					<div class="animation start-home"></div>
				</nav>
			</div>
		</div>
	</header>

	<form action="Post3Result" method="post">
		<h1 Style="text-align: center;">ご利用可能なアメニティと設備</h1>
		<h3 style="text-align: center;">こちらはゲストが泊まりにきた際に利用可能なものを選択してください</h3>
		<div class="selectbox">
			<div class="group">
				<c:forEach var="amenity" items="${amenityList}">
					<input type="checkbox" name="checkbox" value="${amenity.id}"
						id="check${amenity.id}"  class="checkmark">
					<label for="check${amenity.id}" ></label>
					<p>${amenity.itemName}</p>
					<br>
				</c:forEach>
			</div>
          <!--   <div class="group">
                <input type="checkbox" name="checkbox" value="WIFI" id="check2" hidden>
                <label for="check2" class="checkmark"></label>
                <p>WIFI</p>
            </div> -->
		</div>

		<div class="buttonbox">
			<div class="backbox">
				<a href="Post2"><button type="button" class="backbutton">戻る</button></a>
			</div>

			<div class="nextbox">
				<a href="Post3Result"><button type="submit" class="nextbutton">次へ</button></a>
			</div>
		</div>
	</form>
</body>
</html>