<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>post4</title>
    <link rel="stylesheet" href="css/post4.css">
    <script type="text/javascript" src="js/index.js"></script>
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                   <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <h1>掲載する建物やお部屋の写真</h1>
    <form action="Post4Result" method="post" enctype="multipart/form-data">
        <div class="uploadbox">
            <div class="form-input">
                <label for="file1">chose image</label>
                <input type="file" id="file1" name="file1" accept="image/*" onchange="showPreview(event, 'file1-preview')" style="display: none">
                <div class="preview1">
                    <img id="file1-preview" alt="一個目の画像">
                </div>
            </div>
        </div>

        <div class="uploadbox">
            <div class="form-input">
                <label for="file2">chose image</label>
                <input type="file" id="file2" name="file2" accept="image/*" onchange="showPreview(event, 'file2-preview')" style="display: none">
                <div class="preview2">
                    <img id="file2-preview" alt="二個目の画像">
                </div>
            </div>
        </div>

        <div class="uploadbox">
            <div class="form-input">
                <label for="file3">chose image</label>
                <input type="file" id="file3" name="file3" accept="image/*" onchange="showPreview(event, 'file3-preview')" style="display: none">
                <div class="preview3">
                    <img id="file3-preview" alt="三個目の画像">
                </div>
            </div>
        </div>

        <div class="uploadbox">
            <div class="form-input">
                <label for="file4">chose image</label>
                <input type="file" id="file4"name="file4" accept="image/*" onchange="showPreview(event, 'file4-preview')" style="display: none">
                <div class="preview4">
                    <img id="file4-preview" alt="四個目の画像">
                </div>
            </div>
        </div>

        <div class="buttonbox">
        <c:if test="${errMsg != null }">
        <P style="color: red;">${errMsg}</P>
        </c:if>
            <div class="backbox">
                <a href="Post3"><button type="button" class="backbutton">戻る</button></a>
            </div>

            <div class="nextbox">
                <a href="Post4Result"><button type="submit" class="nextbutton">次へ</button></a>
            </div>
        </div>

    </form>
</body></html>