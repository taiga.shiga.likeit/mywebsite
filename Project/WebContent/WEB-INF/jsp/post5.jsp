<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>post5</title>
    <link rel="stylesheet" href="css/post5.css">
    <script type="text/javascript" src="js/index.js"></script>
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
        <h1>ゲストが泊まれる期間の指定してください。</h1>
        <form action="Post5Result" method="post">
            <div class="form-box">
                <p>チェックイン</p>
                <input type="date" class="checkin-day"  name="checkIn" value="${checkIn}">
                <p>チェックアウト</p>
                <input type="date" class="checkout-day" name="checkOut" value="${checkOut}">
            </div>

            <div class="buttonbox">
             <c:if test="${errMsg != null}">
             <p style="color:red">${errMsg}</p>
             </c:if>
                <div class="backbox">
                    <a href="Post4"><button type="button" class="backbutton">戻る</button></a>
                </div>

                <div class="nextbox">
                    <a href="Post5Result"><button type="submit" class="nextbutton">次へ</button></a>
                </div>
            </div>
        </form>
    </header>
</body></html>