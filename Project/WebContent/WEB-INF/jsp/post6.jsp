<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="css/post6.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <form action="Post6Result" method="post">
        <h1>最後にゲストへのメッセージをお願いします</h1>
        <div>
            <p>お部屋のおおまかな特徴や<br>ゲストとのコミュニケーションの<br>頻度や方法に関する説明を記入してください。</p>
            <p><textarea name="message" cols="500" rows="10" placeholder="例 ルールが守れる人　清潔感のあるひとのみ。　騒ぐのは禁止">こんにちは</textarea></p>
        </div>

        <div class="buttonbox">
            <div class="backbox">
                <a href= "Post5"><button type="button" class="backbutton">戻る</button></a>
            </div>
            <div class="nextbox">
                <a href="Post6Result"><button type="submit" class="nextbutton">掲載</button></a>
            </div>
        </div>

    </form>
</body></html>