<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>詳細</title>
    <link rel="stylesheet" href="css/profile.css">
</head>

<body>
<c:if test="${update != null}">
 <script >
   alert('${update}');
 </script>
</c:if>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <h1 style="text-align: center">ユーザー情報</h1>
   <div class="box">
    <form class="setting" action="UserUpdate" method="post" enctype="multipart/form-data">
        <input type="file" name="file"onchange="preview(event)" accept="image/*" id="change" style="display: none" >
        <img src="img/${user.userImage}" id="img"><br>
        <label for="change">プロフィール写真を変更</label>
        <div class="textbox">
            <label>ログインID</label>
            <input type="text" placeholder="LoginID" name="login_id" value="${user.loginId}" class="text1">
        </div>
        <div class="textbox">
            <label>名前</label>
            <input type="text" placeholder="name" name="name" value="${user.name}" class="text1">
        </div>
        <div class="textbox">
            <label>パスワード</label>
            <input type="text" placeholder="password" name="password" value="" class="text1">
        </div>
          <c:if test="${errMsg != null }">
            <p>${errMsg}</p>
            </c:if>
        <button class="button btn1" type=submit name="action">更新</button>
    </form>

    <div class="next">
       <a href="FavoriteList" ><button class="button btn1" type=submit name="action">お気にいりを見る</button></a>
       <a href="BookList"><button class="button btn1" type=submit name="action" style="margin-top: 50px;">予約を確認</button></a>
    </div>
</div>

    <script>
        function preview(event) {
            var input = event.target.files[0];
            var reader = new FileReader();
            reader.onload = function() {
                var result = reader.result;
                var img = document.getElementById('img');
                console.log(img);
                img.src = result;
            }
            reader.readAsDataURL(input);
        }
    </script>
</body></html>