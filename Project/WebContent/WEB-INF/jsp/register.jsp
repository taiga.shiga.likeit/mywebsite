<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html lang="ja">

<head>
    <meta charset="UTF-8">
    <title>新規登録</title>
    <link rel="stylesheet" href="css/register.css">
</head>

<body>
    <header>
        <div class="container">
            <div class="header-list">
                <nav>
                    <a href="Home">HOME</a>
                    <a href="Post1">POST</a>
                    <a href="Message">MESSAGE</a>
                    <a href="Profile">PROFILE</a>
                    <a href="Regist">NEWUSER</a>
                    <a href="Logout">LOGOUT</a>
                    <div class="animation start-home"></div>
                </nav>
            </div>
        </div>
    </header>

    <div>
        <p class="image1"><img src="img/people_PNG.png" alt="人々のイメージ"></p>
        <form action="RegistResult" method="post">
            <div class="login-box">
                <h1>Sing up</h1>
                <p id="name">名前</p>
                <div class="textbox">
                    <input type="text" placeholder="Username" name="name" value="" class="text1">
                </div>

                <p id="loginID">ログインID</p>
                <div class="textbox">
                    <input type="text" placeholder="LoginID" name="login_id" value="" class="text2">
                </div>

                <p id="password">パスワード</p>
                <div class="textbox">
                    <input type="text" placeholder="Password" name="password" value="" class="text3">
                </div>

                <p id="conform-password">確認用パスワード</p>
                <div class="textbox">
                    <input type="text" placeholder="確認用Password" name="conform_pw" value="" class="text4">
                </div>
                <c:if test="${errMsg != null}">
	 			<p style="color: red;">${errMsg}</p>
				 </c:if>
                <button class="button btn1" type=submit name="action">新規登録</button>
            </div>
        </form>
    </div>

</body>

</html>
