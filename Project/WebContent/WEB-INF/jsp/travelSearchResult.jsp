<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">

<head>
<meta charset="UTF-8">
<title>検索結果</title>
<link rel="stylesheet" href="css/travelSearchResul.css">
</head>

<body>
	<header>
		<div class="container">
			<div class="header-list">
				<nav>
					<a href="Home">HOME</a> <a href="Post1">POST</a> <a href="Message">MESSAGE</a>
					<a href="Profile">PROFILE</a> <a href="Regist">NEWUSER</a> <a
						href="Logout">LOGOUT</a>
					<div class="animation start-home"></div>
				</nav>
			</div>
		</div>
	</header>

	<h2>検索ワード ${searchWord}</h2>
	<p class="resultnumber" style="padding-left: 200px;">検索結果${postCount}件</p>


	<div class="seciton">
   <c:forEach var="post" items="${postList}">
	 	<div class="card">
			<div class="card_image">
				<a href="SearchDetail?postId=${post.id}&userId=${post.userId}"><img src="img/${post.mainImgName}"></a>
			</div>
			<div class="hosting_content">
				<span class="hosting-type">建物スタイル ${post.buildingId}</span>
				<p>貸し出しスタイル  ${post.roomId}</p>
			</div>
		</div>
	</c:forEach>

		<!-- <div>
			<div class="pagination">
				<a href="#">&laquo;</a> <a href="#" class="active">1</a> <a href="#">2</a>
				<a href="#">3</a> <a href="#">4</a> <a href="#">5</a> <a href="#">6</a>
				<a href="#">&raquo;</a>
			</div>
		</div> -->

	</div>
</body>
</html>
