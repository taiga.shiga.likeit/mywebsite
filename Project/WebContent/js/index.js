function showPreview(event, targetId) {
    if (event.target.files.length > 0) {
        console.log(event.target.files.length);
        var src = URL.createObjectURL(event.target.files[0]);
        var preview = document.getElementById(targetId);
        preview.src = src;
        preview.style.display = "block";
    }
}
