package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBManager {
	final private static String URL = "jdbc:mysql://localhost/myWebSite?useUnicode=true&characterEncoding=utf8";
    final private static String USER = "root";
    final private static String PASS = "password";

	public static Connection getConnection() {
		Connection con = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			con = DriverManager.getConnection(URL, USER , PASS);
			System.out.println("DBConnected!!");
			return con;

		} catch (ClassNotFoundException e) {
			throw new IllegalMonitorStateException();

		} catch (SQLException e) {
			throw new IllegalMonitorStateException();
		}
	}
}
