package beans;

import java.io.Serializable;

public class AmenityDatabeans  implements Serializable {

	private int id;
	private String itemName;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}


}
