package beans;

import java.io.Serializable;

public class BuildingTypeDataBeans  implements Serializable{


	private int id;
	private String buildingType;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getBuildingType() {
		return buildingType;
	}
	public void setBuildingType(String buildingType) {
		this.buildingType = buildingType;
	}
}
