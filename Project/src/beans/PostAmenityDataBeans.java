package beans;

import java.io.Serializable;

public class PostAmenityDataBeans  implements Serializable{

	private int id;
	private int postid;
	private int amenityId;

	//後で記述していく
	public PostAmenityDataBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPostid() {
		return postid;
	}

	public void setPostid(int postid) {
		this.postid = postid;
	}

	public int getAmenityId() {
		return amenityId;
	}

	public void setAmenityId(int amenityId) {
		this.amenityId = amenityId;
	}


}
