package beans;

import java.io.Serializable;

public class RoomTypeDataBeans  implements Serializable{

	private int id;
	private String roomType;

	public RoomTypeDataBeans() {
		this.roomType = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoomType() {
		return roomType;
	}

	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}


}
