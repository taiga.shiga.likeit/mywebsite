package beans;

import java.io.Serializable;
import java.sql.Date;

public class UserDataBeans  implements Serializable {

	private int id;
	private String name;
	private String loginId;
	private String password;
	private String userImage;
	private Date createDate;

	// ログイン時に使用
	// session用で loginIdとname と userImageを持しておく
	public UserDataBeans(String loginId, String name, int userId, String userImg) {
		this.id = userId;
		this.name = name;
		this.loginId = loginId;
		this.userImage = userImg;
	}


	// ユーザーデータ更新時の情報をまとめて持たせる。
	public void setUpdateUserDataBeansInfo(String name, String loginId, String userImg) {
		this.name = name;
		this.loginId = loginId;
		this.userImage = userImg;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getUserImage() {
		return userImage;
	}
	public void setUserImage(String userImage) {
		this.userImage = userImage;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

}
