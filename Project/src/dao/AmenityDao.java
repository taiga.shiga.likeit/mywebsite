package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.AmenityDatabeans;

public class AmenityDao {

	// アメニティーの情報を全部持ってくる
	public static ArrayList<AmenityDatabeans> getALLAmenity() {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM amenity";
			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			ArrayList<AmenityDatabeans> amenityDB = new ArrayList<AmenityDatabeans>();
			// 全部取ってくる
			while (rs.next()) {
				AmenityDatabeans amdb = new AmenityDatabeans();
				amdb.setId(rs.getInt("id"));
				amdb.setItemName(rs.getString("item_name"));
				amenityDB.add(amdb);
			}
			return amenityDB;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// postIDでアメニティの情報をとってくる
		public static ArrayList<AmenityDatabeans> getAmenityList(int postId) {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				String sql = "SELECT amenity.item_name FROM post_amenity INNER JOIN amenity ON post_amenity.amenity_id = amenity.id WHERE post_amenity.post_id = ?";
				st = con.prepareStatement(sql);
				st.setInt(1, postId);
				ResultSet rs = st.executeQuery();
				ArrayList<AmenityDatabeans>amenityList = new ArrayList<AmenityDatabeans>();
				while(rs.next()) {
					AmenityDatabeans amenityDB = new AmenityDatabeans();
					String itemName = rs.getString("item_name");
					amenityDB.setItemName(itemName);
					amenityList.add(amenityDB);
				}
				// ameinityListをここで追加
				return amenityList;
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
				return null;
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
}
