package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import base.DBManager;

public class BookingDao {

	// 自分自身のところに予約があるか確認
		public static int bookIsExist(int userId, int postId) {
			Connection con = null;
			PreparedStatement st = null;
			try {
				int isExsistNum = 0;
				con = DBManager.getConnection();
				String sql = "SELECT * FROM booking where user_id = ? AND post_id = ? ";
				st = con.prepareStatement(sql);
				st.setInt(1, userId);
				st.setInt(2, postId);
				ResultSet rs = st.executeQuery();
				// お気に入りがない場合は
				if (!rs.next()) {
					return isExsistNum;
				}
				// お気に入り情報がある場合は
				isExsistNum++;
				return isExsistNum;
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
				return 0;
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}


		// 予約を登録する
		public static void insertBook(int userId, int postId) {
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				String sql = "INSERT INTO booking (user_id, post_id) VALUES (?,?)";
				st = con.prepareStatement(sql);
				st.setInt(1, userId);
				st.setInt(2, postId);
				st.executeUpdate();
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
			} finally {
				// データベース切断
				if (con != null) {
					try {
						con.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
}
