package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.BuildingTypeDataBeans;

public class BuildingTypeDao {

	// DBに登録されている建物情報を持ってくる
	public static ArrayList<BuildingTypeDataBeans> getAllBuildingType() {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM building_type";
			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			// リストとして持たせる
			ArrayList<BuildingTypeDataBeans> buildingTypeDB = new ArrayList<BuildingTypeDataBeans>();
			//全部取ってくる
			while (rs.next()) {
			BuildingTypeDataBeans btdb = new BuildingTypeDataBeans();
			btdb.setBuildingType(rs.getString("building_type"));
			btdb.setId(rs.getInt("id"));
			buildingTypeDB.add(btdb);
			}
			return buildingTypeDB;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
