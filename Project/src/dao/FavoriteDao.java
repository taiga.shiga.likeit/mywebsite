package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.PostDateBeans;

public class FavoriteDao {

	// 自分自身のお気に入りが存在するか確認
	public static int likeIsExist(int userId, int postId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			int isExsistNum = 0;
			con = DBManager.getConnection();
			String sql = "SELECT * FROM favorite where user_id = ? AND post_id = ? ";
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			st.setInt(2, postId);
			ResultSet rs = st.executeQuery();
			// お気に入りがない場合は
			if (!rs.next()) {
				return isExsistNum;
			}
			// お気に入り情報がある場合は
			isExsistNum++;
			return isExsistNum;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// お気に入りを登録する
	public static void insertLike(int userId, int postId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "INSERT INTO favorite (user_id, post_id) VALUES (?,?)";
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			st.setInt(2, postId);
			st.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	//自分自身のお気に入りしたデータを持ってくる。
	public static ArrayList<PostDateBeans> getLikeList(int userId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT post.id, post.building_id, post.room_id, post.file_name from favorite INNER JOIN post ON favorite.post_id = post.id WHERE favorite.user_id = ?";
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			ResultSet rs = st.executeQuery();
			ArrayList<PostDateBeans> likeList = new ArrayList<PostDateBeans>();
			while(rs.next()) {
				 PostDateBeans postDB =  new PostDateBeans();
				 postDB.setId(rs.getInt("id"));
				 postDB.setBuildingId(rs.getString("building_id"));
				 postDB.setRoomId(rs.getString("room_id"));
				 postDB.setMainImgName(rs.getString("file_name"));
				 likeList.add(postDB);
			}

			return likeList;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
	}

	// お気に入り削除
	public static void delateLike(int userId, int postId) {
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			String sql = "DELETE FROM favorite WHERE user_id = ? AND post_id = ?";
			st = con.prepareStatement(sql);
			st.setInt(1, userId);
			st.setInt(2, postId);
			st.executeUpdate();
		}catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}
 }
}
