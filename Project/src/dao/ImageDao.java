package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Statement;

import base.DBManager;
import beans.ImageDataBeans;

public class ImageDao {

	// 画像の登録
	// id  , image_name , post_id
	public static int insertPostImage(String imageName) {
		Connection con = null;
		PreparedStatement st = null;
		int imageid = 0;
		try {
			con = DBManager.getConnection();
			String sql = "INSERT INTO image(image_name) VALUES(?)";
			st = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				st.setString(1, imageName);
				st.executeUpdate();
				// 登録した情報を持ってくる
				ResultSet rs = st.getGeneratedKeys();
				if(rs.next()) {
				imageid = rs.getInt(1);
				}
			System.out.println("image登録完了");
		   return imageid;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return 0;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// postIDにより投稿されたimageをとってくる
		public static ArrayList<ImageDataBeans> getPostImages(int postId){
			Connection con = null;
			PreparedStatement st = null;
			try {
				con = DBManager.getConnection();
				String sql = "SELECT image.image_name FROM post_images INNER JOIN image ON post_images.image_id = image.id WHERE post_images.post_id = ?";
				st = con.prepareStatement(sql);
				st.setInt(1, postId);
				ResultSet rs = st.executeQuery();
				ArrayList<ImageDataBeans> imageList = new ArrayList<ImageDataBeans>();
				while(rs.next()) {
					ImageDataBeans imageDB = new ImageDataBeans();
					String imageName = rs.getString("image_name");
					imageDB.setImageName(imageName);
					imageList.add(imageDB);
				}
				return imageList;
			} catch (Exception e) {
				// TODO: handle exception
				System.out.println(e.getMessage());
				return null;
			}finally {
				if(con != null) {
					try {
						con.close();
					}catch (SQLException e) {
						// TODO: handle exception
						e.printStackTrace();
					}
				}
			}
		}

}
