package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import base.DBManager;

public class PostAmenityDao {

	// postAmenityテーブルに保存していく
	public static void insertPostAmenityData(int postId, int amenityId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "INSERT INTO post_amenity (post_id, amenity_id) VALUES (? , ?)";
			st = con.prepareStatement(sql);
			st.setInt(1, postId);
			st.setInt(2, amenityId);
			st.executeUpdate();
			System.out.println("PostAmenityに追加");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
