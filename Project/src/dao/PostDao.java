package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;

import base.DBManager;
import beans.PostDateBeans;

public class PostDao {

	// postテーブルへの登録
	public static int insertYourHouse(int userId, PostDateBeans pdb1, PostDateBeans pdb2,
			Date checkIn, Date checkOut, String message, String mainImgName) {
		Connection con = null;
		PreparedStatement st = null;
		int postId = 0;
		try {
			con = DBManager.getConnection();
			//
			String sql = "INSERT INTO post (user_id, building_id, room_id, people_num, postal_num, address, keyword, checkIn, checkOut, message, create_date, file_name) VALUES (?,?,?,?,?,?,?,?,?,?,now(),?)";
			st = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
			// テーブルの各カラムへの登録
			st.setInt(1, userId);
			//投稿１
			st.setString(2, pdb1.getBuildingId());
			st.setString(3, pdb1.getRoomId());
			st.setInt(4, pdb1.getPeopleNum());
			//投稿2
			st.setString(5, pdb2.getPostalNum());
			st.setString(6, pdb2.getAddress());
			st.setString(7, pdb2.getKeyword());
			// sql用のdate型に変換
			st.setDate(8, (java.sql.Date) checkIn);
			st.setDate(9, (java.sql.Date) checkOut);
			st.setString(10, message);
			st.setString(11, mainImgName);
			st.executeUpdate();

			// 登録した情報を持ってくる
			ResultSet rs = st.getGeneratedKeys();
			if (rs.next()) {
				postId = rs.getInt(1);
			}
			System.out.println("postIdは" + postId);
			System.out.println("新規投稿終了");
			return postId;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return postId;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// 検索条件にあう投稿情報をとってくる
	public static ArrayList<PostDateBeans> findPosting(String keyword, String checkIn,
			String checkOut,
			int guestNum) {
		Connection con = null;
		PreparedStatement st = null;
		// 検索した時に出る投稿データを格納するコレクション
		ArrayList<PostDateBeans> postList = new ArrayList<PostDateBeans>();
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM post WHERE id > 0";
			// 検索されたワードに応じてより詳細な検索
			if (!keyword.equals("")) {
				// keywordが部分一致
				sql += " AND keyword LIKE '%" + keyword + "%'";
			}
			if (!checkIn.equals("")) {
				// 宿泊開始日の指定
				sql += " AND checkIn >= '" + checkIn + "'";
			}
			if (!checkOut.equals("")) {
				// 宿泊最終日の指定
				sql += " AND checkOut <= '" + checkOut + "'";
			}
			if (guestNum > 0) {
				sql += " AND people_num = '" + guestNum + "'";
			}

			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();

			// 結果をとってくる
			while (rs.next()) {
				PostDateBeans postDB = new PostDateBeans();
				postDB.setId(rs.getInt("id"));
				postDB.setUserId(rs.getInt("user_id"));
				postDB.setBuildingId(rs.getString("building_id"));
				postDB.setRoomId(rs.getString("room_id"));
				postDB.setPeopleNum(rs.getInt("people_num"));
				postDB.setPostalNum(rs.getString("postal_num"));
				postDB.setAddress(rs.getString("address"));
				postDB.setKeyword(rs.getString("keyword"));
				postDB.setCheckIn(rs.getDate("checkIn"));
				postDB.setCheckOut(rs.getDate("checkOut"));
				postDB.setMessage(rs.getString("message"));
				postDB.setCreateDate(rs.getDate("create_date"));
				postDB.setMainImgName(rs.getString("file_name"));
				// 追加
				postList.add(postDB);
			}
			System.out.println("投稿情報をとってきました");
			return postList;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// 投稿の詳細情報をpostidから持ってくる
	public static PostDateBeans getPostDetail(int postId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM post WHERE id = ?";
			st = con.prepareStatement(sql);
			// postidをセット
			st.setInt(1, postId);
			// 投稿情報をとってくる
			ResultSet rs = st.executeQuery();
			PostDateBeans postDB = new PostDateBeans();
			if (rs.next()) {
				postDB.setId(rs.getInt("id"));
				postDB.setUserId(rs.getInt("user_id"));
				postDB.setBuildingId(rs.getString("building_id"));
				postDB.setRoomId(rs.getString("room_id"));
				postDB.setPeopleNum(rs.getInt("people_num"));
				postDB.setPostalNum(rs.getString("postal_num"));
				postDB.setAddress(rs.getString("address"));
				postDB.setKeyword(rs.getString("keyword"));
				postDB.setCheckIn(rs.getDate("checkIn"));
				postDB.setCheckOut(rs.getDate("checkOut"));
				postDB.setMessage(rs.getString("message"));
				postDB.setCreateDate(rs.getDate("create_date"));
				postDB.setMainImgName(rs.getString("file_name"));
			}
			return postDB;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
