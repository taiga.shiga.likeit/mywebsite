package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.RoomTypeDataBeans;

public class RoomTypeDao {



	// DBに登録されている部屋情報を持ってくる
	public static ArrayList<RoomTypeDataBeans> getAllRoomType() {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM room_type";
			st = con.prepareStatement(sql);
			ResultSet rs = st.executeQuery();
			// リストとして持たせる
			ArrayList<RoomTypeDataBeans> roomTypeDB = new ArrayList<RoomTypeDataBeans>();
			//全部取ってくる
			while (rs.next()) {
			RoomTypeDataBeans rtdb = new RoomTypeDataBeans();
			rtdb.setRoomType(rs.getString("room_type"));
			rtdb.setId(rs.getInt("id"));
			roomTypeDB.add(rtdb);
			}
			return roomTypeDB;
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}
}
