package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.xml.bind.DatatypeConverter;

import base.DBManager;
import beans.UserDataBeans;

public class UserDao {

	// ログイン時のユーザーが存在するか確認
	public static UserDataBeans findByLoginInfo(String loginId, String password) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM user WHERE login_id = ? and password = ?");
			String yourPassword = pwEncryption(password);
			st.setString(1, loginId);
			st.setString(2, yourPassword);
			// sqlのデータがここに入る
			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				System.out.println("あなたのアカウントはありません");
				return null;
			}
			String loginIdData = rs.getString("login_id");
			int userId = rs.getInt("id");
			String userNameData = rs.getString("name");
			String userImg = rs.getString("user_img");
			// 登録成功
			System.out.println("アカウントがありました");
			// userをuserdatabeansで保持
			return new UserDataBeans(loginIdData, userNameData, userId, userImg);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// 新規登録用メソッド
	public static void createUser(String userName, String loginId, String password) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO user(name,login_id,password,user_img,create_date) VALUES(?, ? ,? ,'社長のアイコン.png',now())");
			String yourPassword = pwEncryption(password);
			System.out.println("パスワードが暗号化されました" + yourPassword);
			st.setString(1, userName);
			st.setString(2, loginId);
			st.setString(3, yourPassword);
			st.executeUpdate();
			System.out.println("新規登録成功");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			System.out.println("例外でした");
		} finally {
			// データベースを閉じる
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// ログインidが重複していないかのチェック
	public static String idExist(String loginId) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			String sql = "SELECT * FROM user WHERE login_id = ?";
			st = con.prepareStatement(sql);
			st.setString(1, loginId);
			// 実行
			ResultSet rs = st.executeQuery();
			if (!rs.next()) {
				return null;
			}
			return "idが存在しました";
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	// user情報の更新
	public static UserDataBeans upDateuser(String userName, String loginId, String imageName, int userId, String password) {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			if (password.isEmpty()) {
			String sql = "UPDATE user SET name= ?, login_id= ?, user_img = ? WHERE id= ?";
			st = con.prepareStatement(sql);
			st.setString(1, userName);
			st.setString(2, loginId);
			st.setString(3, imageName);
			st.setInt(4, userId);
			// 更新
			st.executeUpdate();
			System.out.println("更新しました");
			return new UserDataBeans(loginId, userName, userId, imageName);
			} else {
				String sql = "UPDATE user SET name = ? , login_Id = ?, user_img = ?, password = ? where login_id = ?";
				// SELECTを実行し、結果表を取得
				PreparedStatement pStmt = con.prepareStatement(sql);
				String yourPassword = pwEncryption(password);
			    pStmt.setString(1, userName);
			    pStmt.setString(2, loginId);
			    pStmt.setString(3, imageName);
			    pStmt.setString(4, yourPassword);
			    pStmt.setInt(5, userId);
				pStmt.executeUpdate();
				return new UserDataBeans(loginId, userName, userId, imageName);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return null;
		} finally {
			// TODO: handle finally clause
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// パスワードを暗号化するメソッド
	public static String pwEncryption(String password) throws NoSuchAlgorithmException {
		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";
		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);
		return result;
	}

}
