package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDateBeans;
import beans.UserDataBeans;
import dao.FavoriteDao;

/**
 * Servlet implementation class BookList
 */
@WebServlet("/BookList")
public class BookList extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BookList() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UserDataBeans userDB = (UserDataBeans) session.getAttribute("user");
		ArrayList<PostDateBeans> bookList = FavoriteDao.getLikeList(userDB.getId());
		request.setAttribute("bookList",bookList);
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.BOOK_LIST);
		dispatcher.forward(request, response);

	}



}
