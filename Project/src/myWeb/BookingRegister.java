package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AmenityDatabeans;
import beans.ImageDataBeans;
import beans.PostDateBeans;
import beans.UserDataBeans;
import dao.AmenityDao;
import dao.BookingDao;
import dao.ImageDao;
import dao.PostDao;

/**
 * Servlet implementation class BookingRegister
 */
@WebServlet("/BookingRegister")
public class BookingRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookingRegister() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		// idを取得
		int postId = Integer.parseInt(request.getParameter("postId"));
		// ユーザー情報
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		// amenity 情報を持ってくる
		ArrayList<AmenityDatabeans> amenityList = AmenityDao.getAmenityList(postId);
		// imageを持ってくる
		ArrayList<ImageDataBeans> imageList = ImageDao.getPostImages(postId);

		// データを取ってくる
		PostDateBeans postDB = PostDao.getPostDetail(postId);
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.TRAVELDETAIL);
		int isExistNum = BookingDao.bookIsExist(user.getId(), postId);
		switch (isExistNum) {
		case 0:
			System.out.println("予約はありません");
			// 登録
		    BookingDao.insertBook(user.getId(), postId);
			// アメニティ情報
			request.setAttribute("amenityList", amenityList);
			// ポスト情報
			request.setAttribute("post", postDB);
			// postIdにあうimageを取ってくる
			request.setAttribute("imageList", imageList);
			request.setAttribute("alertMsg", "登録されました");
			dispatcher.forward(request, response);
			break;
		case 1:
			System.out.println("予約がありました");
			// アメニティ情報
			request.setAttribute("amenityList", amenityList);
			// ポスト情報
			request.setAttribute("post", postDB);
			// postIdにあうimageを取ってくる
			request.setAttribute("imageList", imageList);
			request.setAttribute("alertMsg", "既に予約されています");
			dispatcher.forward(request, response);
			break;
		}

	}

}
