package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.FavoriteDao;

/**
 * Servlet implementation class FavoriteDelate
 */
@WebServlet("/FavoriteDelate")
public class FavoriteDelate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public FavoriteDelate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		int postId = Integer.parseInt(request.getParameter("postId"));
		FavoriteDao.delateLike(user.getId(), postId);
		// postidにあるアメニティをとってくる
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.HOME_PAGE);
		dispatcher.forward(request, response);
	}


}
