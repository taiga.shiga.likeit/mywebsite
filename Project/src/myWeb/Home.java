package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDateBeans;
import beans.UserDataBeans;
import dao.PostDao;

/**
 * Servlet implementation class Home
 */
@WebServlet("/Home")
public class Home extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//1ページに表示する商品数
	final static int PAGE_MAX_ITEM_COUNT = 8;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Home() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		// ログインをしてない場合は、最初にログインさせる
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.LOGIN_PAGE);
			dispatcher.forward(request, response);
			System.out.println("ログインを最初にしてください");
			return;
		}

		// ログインしている
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.HOME_PAGE);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		String location = request.getParameter("location");
		int guestNum = Integer.parseInt(request.getParameter("guestNum"));
		String checkIn = request.getParameter("checkIn");
		String checkOut =  request.getParameter("checkOut");

		ArrayList<PostDateBeans> postList = PostDao.findPosting(location, checkIn, checkOut, guestNum);
		// 検索結果情報をここに入れる
		request.setAttribute("postList",postList);
		// 検索件数結果数
		request.setAttribute("postCount", postList.size());
		// 検索に使ったキーワード
		request.setAttribute("searchWord", location);
		// 検索結果を表示
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.TRAVELRESULT);
		dispatcher.forward(request, response);
	}

}
