package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class LoginResult
 */
@WebServlet("/LoginResult")
public class LoginResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
			// パラメーターから取得
			String loginId = request.getParameter("login_id");
			String password = request.getParameter("password");

			// ユーザーを取得
			UserDataBeans user = (UserDataBeans) UserDao.findByLoginInfo(loginId, password);
			// nullでdataが帰ってきた場合
			if (user == null ) {
				request.setAttribute("errMsg", "ログインID、パスワードが見つかりません。もう一度お願いします。");
				RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.LOGIN_PAGE);
				dispatcher.forward(request, response);
				return;
			}
			// データとして持たす。
			System.out.println(user.getUserImage());
			session.setAttribute("user", user);
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.HOME_PAGE);
			dispatcher.forward(request, response);
			System.out.println("ログイン成功");
	}

}
