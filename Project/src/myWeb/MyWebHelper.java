package myWeb;

public class MyWebHelper {
	//ログイン
	static final String LOGIN_PAGE = "/WEB-INF/jsp/index.jsp";
	//ホーム
	static final String HOME_PAGE = "/WEB-INF/jsp/home.jsp";
	// 新規登録
	static final String REGIST_PAGE = "/WEB-INF/jsp/register.jsp";
	// 予約詳細ページ
	static final String BOOK_DETAIL = "/WEB-INF/jsp/bookDetail.jsp";
	// 予約一覧
	static final String BOOK_LIST = "/WEB-INF/jsp/bookList.jsp";
	// お気に入り詳細ページ
	static final String LIKE_DETAIL = "/WEB-INF/jsp/likeDetail.jsp";
	// お気に入りリスト
	static final String LIKE_LIST = "/WEB-INF/jsp/likeList.jsp";
	// メッセージボックス
	static final String MESSAGE = "/WEB-INF/jsp/message.jsp";
	// 投稿
	static final String POST1 = "/WEB-INF/jsp/post1.jsp";
	static final String POST2 = "/WEB-INF/jsp/post2.jsp";
	static final String POST3 = "/WEB-INF/jsp/post3.jsp";
	static final String POST4 = "/WEB-INF/jsp/post4.jsp";
	static final String POST5 = "/WEB-INF/jsp/post5.jsp";
	static final String POST6 = "/WEB-INF/jsp/post6.jsp";
	// プロフィール
	static final String  PROFILE = "/WEB-INF/jsp/profile.jsp";
	// 検索結果リスト
	static final String TRAVELRESULT =  "/WEB-INF/jsp/travelSearchResult.jsp";
	// 検索結果詳細
	static final String TRAVELDETAIL =  "/WEB-INF/jsp/travelDetail.jsp";


	public static boolean isLoginIdValidation(String inputLoginId) {
		// 英数字アンダースコア以外が入力されていたら
		if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
			return true;
		}
		return false;
	}

	// 郵便番号かを判定
	public static boolean isPostalValidation(String postalNum) {
		if (postalNum.matches("^[0-9]{3}-[0-9]{4}$")){
			return true;
		}
		return false;
	}


	}
