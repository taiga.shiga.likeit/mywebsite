package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuildingTypeDataBeans;
import beans.RoomTypeDataBeans;
import beans.UserDataBeans;
import dao.BuildingTypeDao;
import dao.RoomTypeDao;

/**
 * Servlet implementation class Post1
 */
@WebServlet("/Post1")
public class Post1 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		// ログインをしてない場合は、最初にログインさせる
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.LOGIN_PAGE);
			dispatcher.forward(request, response);
			System.out.println("ログインを最初にしてください");
			return;
		}

		ArrayList<BuildingTypeDataBeans> buildingTypeDB  = BuildingTypeDao.getAllBuildingType();
		ArrayList<RoomTypeDataBeans> roomTypeDB = RoomTypeDao.getAllRoomType();

		RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST1);

		// リスト型で保持した情報をリクエストスコープとして保持する
		request.setAttribute("buildingType", buildingTypeDB);
		request.setAttribute("roomType", roomTypeDB);

		dispathcer.forward(request, response);
	}


}
