
package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDateBeans;

/**
 * Servlet implementation class Post1Result
 */
@WebServlet("/Post1Result")
public class Post1Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post1Result() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
				request.setCharacterEncoding("UTF-8");
				HttpSession session = request.getSession();
			// パラメーターから取得
				String buidingId = request.getParameter("buildingType_id");
				String roomId = request.getParameter("roomType_id");
				int num =  Integer.parseInt( request.getParameter("number"));

			    PostDateBeans pdb1 = new PostDateBeans();
			     pdb1.setBuildingId(buidingId);
			     pdb1.setRoomId(roomId);
			     pdb1.setPeopleNum(num);

			     session.setAttribute("pdb1", pdb1);

				RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST2);
				dispatcher.forward(request, response);
	}

}
