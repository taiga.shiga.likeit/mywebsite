package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AmenityDatabeans;
import beans.PostDateBeans;
import dao.AmenityDao;

/**
 * Servlet implementation class Post2Result
 */
@WebServlet("/Post2Result")
public class Post2Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post2Result() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

	// パラメーターから取得
		String postalNum = request.getParameter("postalNum");
		String address = request.getParameter("address");
		String keyWord = request.getParameter("keyWord");

		if (postalNum.equals("") || address.equals("") || keyWord.equals("")) {
			request.setAttribute("errMsg", "空欄がありますもう一度お願いします");
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST2);
			dispatcher.forward(request, response);
			return;
		}
		Boolean isPostaNum = MyWebHelper.isPostalValidation(postalNum);

		if (isPostaNum == false) {
			request.setAttribute("errMsg", "郵便番号は数字でお願いします");
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST2);
			dispatcher.forward(request, response);
			return;
		}
		PostDateBeans pdb2 = new PostDateBeans();

		pdb2.setPostalNum(postalNum);
		pdb2.setAddress(address);
		pdb2.setKeyword(keyWord);
		session.setAttribute("pdb2", pdb2);

		ArrayList<AmenityDatabeans> amenityDBList = AmenityDao.getALLAmenity();
		// リストとして持たせる
		request.setAttribute("amenityList", amenityDBList);
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST3);
		dispatcher.forward(request, response);
	}

}
