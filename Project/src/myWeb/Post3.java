package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.AmenityDatabeans;
import dao.AmenityDao;

/**
 * Servlet implementation class Post3
 */
@WebServlet("/Post3")
public class Post3 extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post3() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


		ArrayList<AmenityDatabeans> amenityDBList = AmenityDao.getALLAmenity();
		// リストとして持たせる
		request.setAttribute("amenityList", amenityDBList);
		RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST3);
		dispathcer.forward(request, response);
	}


}
