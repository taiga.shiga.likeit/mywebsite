package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.AmenityDatabeans;
import dao.AmenityDao;

/**
 * Servlet implementation class Post3Result
 */
@WebServlet("/Post3Result")
public class Post3Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post3Result() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		//文字化け対策
				request.setCharacterEncoding("UTF-8");
				HttpSession session = request.getSession();


		        String[] checkedAmenity = request.getParameterValues("checkbox");

		        if (checkedAmenity == null) {
		        	System.out.println("何も入力されませんでした");
		        	request.setAttribute("errMsg", "何も選択されていません");

		    		ArrayList<AmenityDatabeans> amenityDBList = AmenityDao.getALLAmenity();
		    		// リストとして持たせる
		    		request.setAttribute("amenityList", amenityDBList);
		        	RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST3);
					dispathcer.forward(request, response);
		        	return;
		        }
		        // 配列としてここで持たせる。
		        session.setAttribute("amenityList", checkedAmenity);
		    	RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST4);
				dispathcer.forward(request, response);
		}

}
