package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 * Servlet implementation class Post4Result
 */
@WebServlet("/Post4Result")
@MultipartConfig(location = "/Users/shigataiga/Documents/Git/myWebSite/Project/WebContent/img",
maxFileSize = 1048576)

public class Post4Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post4Result() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		// セッション
		HttpSession session = request.getSession();
		// image1
		Part part1 = request.getPart("file1");
		String name1 = this.getFileName(part1);
		// image2
		Part part2 = request.getPart("file2");
		String name2 = this.getFileName(part2);
		//image3
		Part part3 = request.getPart("file3");
		String name3 = this.getFileName(part3);
		// image4
		Part part4 = request.getPart("file4");
		String name4 = this.getFileName(part4);

		if (name1.equals("") || name2.equals("") || name3.equals("") || name4.equals("")) {
			System.out.println("写真が不足しています");
			request.setAttribute("errMsg", "掲載する写真が不足しています");
		 	RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST4);
			dispathcer.forward(request, response);
			return;
		}

		ArrayList<String> imagesList = new ArrayList<String>();

		imagesList.add(name1);
		imagesList.add(name2);
		imagesList.add(name3);
		imagesList.add(name4);

		// コレクション型で持つ
		session.setAttribute("imageList", imagesList);
		// imageの紹介用
		session.setAttribute("fileName", name1);
		for(int i = 0; i < imagesList.size(); i++) {
			System.out.println(i +"番目の" + imagesList.get(i));
		}
		// imgに追加
		 part1.write(name1);
		 part2.write(name2);
		 part3.write(name3);
		 part4.write(name4);

    	RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.POST5);
				dispathcer.forward(request, response);
	}

	// file nameを取ってくる
		private String getFileName(Part part) {
			String name = null;
			for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
				if (dispotion.trim().startsWith("filename")) {
					name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
					name = name.substring(name.lastIndexOf("\\") + 1);
					break;
				}
			}
			return name;
		}

}
