package myWeb;

import java.io.IOException;
import java.sql.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Post5Result
 */
@WebServlet("/Post5Result")
public class Post5Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post5Result() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		// セッション
		HttpSession session = request.getSession();
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST5);

			// 空の場合
		if (request.getParameter("checkIn").equals("") || request.getParameter("checkOut").equals("")){
		 request.setAttribute("errMsg", "入力がされていません");
			dispatcher.forward(request, response);
			return;
		}

		Date checkInDay = Date.valueOf(request.getParameter("checkIn"));
		Date checkOutDay = Date.valueOf(request.getParameter("checkOut"));

		// 日付が前の場合
		if (checkOutDay.before(checkInDay)) {
			   request.setAttribute("errMsg", "チェックアウト日が正しくありません");
			   dispatcher.forward(request, response);
				return;
		// 日付が一緒の場合
		} else if (checkInDay.equals(checkOutDay)) {
			   request.setAttribute("errMsg", "日付が同じです");
			   dispatcher.forward(request, response);
				return;
		}

		session.setAttribute("checkIn", checkInDay);
		session.setAttribute("checkOut", checkOutDay);

		//  post6へ
		dispatcher = request.getRequestDispatcher(MyWebHelper.POST6);
		dispatcher.forward(request, response);
	}

}
