package myWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.PostDateBeans;
import beans.UserDataBeans;
import dao.ImageDao;
import dao.PostAmenityDao;
import dao.PostDao;
import dao.PostImageDao;

/**
 * Servlet implementation class Post6Result
 */
@WebServlet("/Post6Result")
public class Post6Result extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Post6Result() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		// セッション
		HttpSession session = request.getSession();
		UserDataBeans udb= (UserDataBeans)session.getAttribute("user");
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.POST6);
		String message = request.getParameter("message");
		// からの場合
		if(message.equals("")) {
			request.setAttribute("errMsg", "メッセージが未記入です");
			dispatcher.forward(request, response);
			return;
		}
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		// userid
		System.out.println("userid" + user.getId());
		// post１のsession
		PostDateBeans pdb1 = (PostDateBeans) session.getAttribute("pdb1");
		// pdb2 のsession
		PostDateBeans pdb2 = (PostDateBeans) session.getAttribute("pdb2");
		// amenityList
	   String[] amenityList = (String[]) session.getAttribute("amenityList");
		// arraylist型にキャスト 警告が出ている
		ArrayList<String> imageList = (ArrayList<String>) session.getAttribute("imageList");
		Date checkIn = (Date)session.getAttribute("checkIn");
		Date checkOut = (Date)session.getAttribute("checkOut");
		// mainで表示する写真
		String mainImgName = (String)session.getAttribute("fileName");

		// 画像の投稿
		ArrayList<Integer> imageIdList = new ArrayList<Integer>();
		// imageの追加
		for(int i = 0; i < imageList.size(); i++) {
			  int imageid = ImageDao.insertPostImage(imageList.get(i));
			  // 返ってきたidを追加
			  imageIdList.add(imageid);
			  System.out.println(imageid + "イメージidで");
		}
		// postテーブルへの登録
		// 投稿の際のpostidを取得してくる
		int postId = PostDao.insertYourHouse(user.getId(), pdb1, pdb2, checkIn, checkOut, message, mainImgName);
		// postAmenityテーブルへfor文で追加
		for (int i = 0; i < amenityList.length; i++) {
			int amenityId = Integer.parseInt(amenityList[i]);
			PostAmenityDao.insertPostAmenityData(postId, amenityId);
		}
		// postImageテーブルへfor文で追加
		for (int i = 0; i < imageIdList.size(); i++) {
			PostImageDao.insertPostAmenityData(postId, imageIdList.get(i));
		}

		// 投稿1
		session.removeAttribute("pdb1");
		//投稿2
		session.removeAttribute("pdb2");
		// 投稿3
		session.removeAttribute("amenityList");
		// 投稿4
		session.removeAttribute("imageList");
		// 投稿5
		session.removeAttribute("checkIn");
		session.removeAttribute("checkOut");
		// ホーム画面へ
	    dispatcher = request.getRequestDispatcher(MyWebHelper.HOME_PAGE);
	    dispatcher.forward(request, response);
	}

}
