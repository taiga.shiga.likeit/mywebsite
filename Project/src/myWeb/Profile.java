package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;

/**
 * Servlet implementation class Profile
 */
@WebServlet("/Profile")
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Profile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		// ログインをしてない場合は、最初にログインさせる
		if (user == null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.LOGIN_PAGE);
			dispatcher.forward(request, response);
			System.out.println("ログインを最初にしてください");
			return;
		}
		RequestDispatcher dispathcer = request.getRequestDispatcher(MyWebHelper.PROFILE);
		dispathcer.forward(request, response);
	}

}
