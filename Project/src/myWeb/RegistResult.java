package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class RegistResult
 */
@WebServlet("/RegistResult")
public class RegistResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegistResult() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();

		String loginId = request.getParameter("login_id");
		String userName = request.getParameter("name");
		String password = request.getParameter("password");
		String conformPw = request.getParameter("conform_pw");
		// 空の場合
		if (loginId.equals("") || userName.equals("") || password.equals("") || conformPw.equals("")) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.REGIST_PAGE);
			dispatcher.forward(request, response);
			System.out.println("項目の記述が抜けています");
			request.setAttribute("errMsg", "入力されていない項目があります");
			return;
			// パスワードとチェックしたパスワードが違う場合
		} else if (!password.equals(conformPw)){
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.REGIST_PAGE);
			dispatcher.forward(request, response);
			request.setAttribute("errMsg", "入力されていない項目があります");
			System.out.println("パスワードが一致しません");
			return;
		}

		String idIsExist = UserDao.idExist(loginId);
		System.out.println("ユーザーは" + idIsExist);

		// idが既に存在していた場合
		if (idIsExist != null) {
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.REGIST_PAGE);
			dispatcher.forward(request, response);
			request.setAttribute("errMsg", "idが重複しています");
			return;
		}

		// 新規登録
		UserDao.createUser(userName, loginId, password);
		// 新規登録の後 userデータをセットs
		UserDataBeans user = UserDao.findByLoginInfo(loginId, password);
		session.setAttribute("user", user);
		// ホームへ
		request.getRequestDispatcher(MyWebHelper.HOME_PAGE).forward(request, response);
	}
}
