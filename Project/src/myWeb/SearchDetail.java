package myWeb;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.AmenityDatabeans;
import beans.ImageDataBeans;
import beans.PostDateBeans;
import dao.AmenityDao;
import dao.ImageDao;
import dao.PostDao;

/**
 * Servlet implementation class SearchDetail
 */
@WebServlet("/SearchDetail")
public class SearchDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//文字化け対策
		request.setCharacterEncoding("UTF-8");
		// パラメータのid
		int postId = Integer.parseInt(request.getParameter("postId"));
		// amenity 情報を持ってくる
		ArrayList<AmenityDatabeans> amenityList = AmenityDao.getAmenityList(postId);
		// imageを持ってくる
		ArrayList<ImageDataBeans> imageList = ImageDao.getPostImages(postId);
		// データを取ってくる
		PostDateBeans postDB = PostDao.getPostDetail(postId);
		// アメニティ情報
		request.setAttribute("amenityList", amenityList);
		// ポスト情報
		request.setAttribute("post", postDB);
		// postIdにあうimageを取ってくる
		request.setAttribute("imageList", imageList);
		// postidにあるアメニティをとってくる
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.TRAVELDETAIL);
		dispatcher.forward(request, response);
	}

}
