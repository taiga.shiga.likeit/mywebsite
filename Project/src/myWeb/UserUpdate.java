package myWeb;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class ProfileResult
 */
@WebServlet("/UserUpdate")
// 画像投稿用
@MultipartConfig(location = "/Users/shigataiga/Documents/Git/myWebSite/Project/WebContent/img",
		maxFileSize = 1048576)

public class UserUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserUpdate() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		/*文字化け対策*/
		request.setCharacterEncoding("UTF-8");
		// セッション
		HttpSession session = request.getSession();
		// sessionのデータを持ってくる
		UserDataBeans user = (UserDataBeans) session.getAttribute("user");
		String userName = request.getParameter("name");
		String loginId = request.getParameter("login_id");
		String password = request.getParameter("password");

		// ログインidが半角英数字かを判定
		boolean IsloginID = MyWebHelper.isLoginIdValidation(loginId);

		// trueなら
		if (IsloginID == false) {
			System.out.println("英数字以外が入力されました");
			request.setAttribute("errMsg", "英数字アンダースコア以外は入力できません");
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.PROFILE);
			dispatcher.forward(request, response);
			return;
		}

		if (userName.isEmpty() || loginId.isEmpty()) {
			// 値が入ってない場合は、更新はできない
			System.out.println("値が入っていません");
			request.setAttribute("errMsg", "名前もしくはログインIDが正しく入っていませんでした");
			RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.PROFILE);
			dispatcher.forward(request, response);
			return;
		}

		// 画像を登録する処理
		Part part = request.getPart("file");
		String name = this.getFileName(part);
		if(name.equals("")) {
			name = user.getUserImage();
		}
		// udbに格納
		UserDataBeans udb = UserDao.upDateuser(userName, loginId, name, user.getId(), password);
		 part.write(name);
		 System.out.println("写真のパスは" + name);
		 // セッションを更新
		session.setAttribute("user", udb);
		request.setAttribute("update", "更新されました");
		RequestDispatcher dispatcher = request.getRequestDispatcher(MyWebHelper.PROFILE);
		dispatcher.forward(request, response);
	}

	// file nameを取ってくる
	private String getFileName(Part part) {
		String name = null;
		for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
			if (dispotion.trim().startsWith("filename")) {
				name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
				name = name.substring(name.lastIndexOf("\\") + 1);
				break;
			}
		}
		return name;
	}

}
