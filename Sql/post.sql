CREATE TABLE post
( id SERIAL primary key,
 user_id int not null,
 message varchar(256) not null,
 address varchar(256) not null,
 postalNum varchar(256) not null,
 keyword varchar(256) not null,
 check_in date not null,
 check_out date not null,
 house_image_id int not null,
 house_room_id int not null,
 house_type_id int not null,
 amenity_id int not null,
 create_date datetime not null
);





